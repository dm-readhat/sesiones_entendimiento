#!/bin/bash

if [ -z $1 ] ; then
  echo "URL Decision Manager requerida!" && exit 1;
fi

if [ -z $2 ] ; then
  echo "Usuario DM requerido!" && exit 2;
fi

if [ -z $3 ] ; then
  echo "Password Decision Manager requerido!" && exit 1;
fi

if [ -z $4 ] ; then
  echo "Identificador Server Template Requerido!" && exit 2;
fi

if [ -z $5 ] ; then
  echo "Identificador de grupo para paquete de reglas valor requerido!" && exit 1;
fi

if [ -z $6 ] ; then
  echo "Identificador de paquete de reglas valor requerido!" && exit 2;
fi

if [ -z $7 ] ; then
  echo "Identificador de versión para el paquete de reglas valor requerido!" && exit 2;
fi

echo "Datos Validados se procede a realizar despliegue"

URL=$1/rest/controller/management/servers/$4/containers/$6_$7
URL=$(echo $URL | sed 's|//|/|g') 

echo $URL
 
HTTP_CODE=$(curl -k -so /dev/null -w '%{response_code}' -m 300 -X PUT -u $2:$3 $URL -H "accept: application/json" -H "content-type: application/json" -d "{  \"container-id\" : \"$6_$7\",  \"container-name\" : \"$6\",  \"server-template-key\" : null,  \"release-id\" : {    \"group-id\" : \"$5\",    \"artifact-id\" : \"$6\",    \"version\" : \"$7\"  },  \"configuration\" : {  },  \"status\" : \"STARTED\"}")

echo -e "\n"
echo Codigo de respuesta $HTTP_CODE

if [ -z $HTTP_CODE ] || [ $HTTP_CODE != '201' -a $HTTP_CODE != '000' ]
then
 echo "No se logro comunicar con el controlador o se presentento un error al realizar el despliegue de modulo de reglas" && exit 2;
fi

URL_VERIFICACION=$1/rest/controller/runtime/servers/$4/containers/$6_$7/instances
URL_VERIFICACION=$(echo $URL_VERIFICACION | sed 's|//|/|g')

echo "Verificando el despliegue del modulo de reglas en el controlador URL=$URL_VERIFICACION"

while true
do

HTTP_CODE_VERIFICACION=$(curl -k -so /dev/null -w '%{response_code}' -X GET -u $2:$3 -H "accept: application/json" $URL_VERIFICACION)
echo Codigo de respuesta $HTTP_CODE_VERIFICACION

if [ -z $HTTP_CODE_VERIFICACION ] || [ $HTTP_CODE_VERIFICACION -eq '200' ] 
then
 VERIFICACION=$(curl -k -X GET -u $2:$3 -s -H  "accept: application/json"  $URL_VERIFICACION )
 echo $VERIFICACION | python3 -m json.tool
 ESTADO_INICIO=$(echo $VERIFICACION | grep STARTED | wc -l)
 echo $ESTADO_INICIO
 if [ -n $ESTADO_INICIO ] && [ $ESTADO_INICIO  -eq 1 ]
 then
   echo "Contenedor iniciado correctamente"	
   exit 2;
 fi
fi
sleep 60;
done



